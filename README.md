# UGaPST

UGaPST is a tool for reading and analyzing game packages.

It works as a localhost proxy; it connects to a specified port for the server and client
and forwards packets from either side.
