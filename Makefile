all: clean build/main

clean:
	rm -rf build/*

build/main:
	gcc src/*.c -o build/main -Wall -pedantic -g3
