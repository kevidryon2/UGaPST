#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdbool.h>

int client_port = -1;
int server_port = -1;
int buffer_size = BUFSIZ;

FILE *log_file;
bool logging = false;

bool bin_mode = false;

bool isascii(char c) {
	return (c >= 0x20) && (c < 0x7f);
}

void usage() {
	printf("Usage: ugapst -s <server_port> [args]\n");
	printf("	-c <port>            Specify client port\n");
	printf("	-s <port>            Specify server port\n");
	printf("	-d <size>            Specify data buffer size\n");
	printf("	-l <file>            Specify log file\n");
	printf("	-b                   Enable binary mode\n");
	printf("	-h                   Show usage\n");
	exit(1);
}

void parse_options(int argc, char **argv) {
	for (int i=1; i<argc; i++) {
		if (argv[i][0] == '-') {
			for (int j=1; j<strlen(argv[i]); j++) {
				switch (argv[i][j]) {
					case 'c': i++; client_port = strtol(argv[i], NULL, 0); goto skip_next_args;
					case 's': i++; server_port = strtol(argv[i], NULL, 0); goto skip_next_args;
					case 'd': i++; buffer_size = strtol(argv[i], NULL, 0); goto skip_next_args;
					case 'l': i++; log_file = fopen(argv[i], "w"); logging = true; goto skip_next_args;
					case 'b': bin_mode = true; break;
					case 'h': usage(); break;
				}
			}
			
			skip_next_args:
		}
	}
	
	if (server_port == -1) {
		usage();
	}
}

int init_client() {
	//Create new socket
	int proxy_fd = socket(AF_INET, SOCK_STREAM | SOCK_NONBLOCK, 0);
	if (!proxy_fd) {
		perror("socket");
		exit(1);
	}
	
	//Create sockaddr_in with client port
	struct sockaddr_in addr = (struct sockaddr_in){
		AF_INET,
		htons(client_port),
		0
	};
	
	//Bind it
	bind(proxy_fd, &addr, sizeof addr);
	if (!proxy_fd) {
		perror("bind");
		exit(1);
	}
	
	//Start listening
	listen(proxy_fd, 256);
	
	return proxy_fd;
}

int init_server() {
	//Create new socket
	int server_fd = socket(AF_INET, SOCK_STREAM | SOCK_NONBLOCK, 0);
	
	return server_fd;
}

void dumparr(unsigned char *a, int l) {
	for (int i=0; i<l; i++) {
		if (bin_mode) {
			printf("%02X ", a[i]);
			if (logging) if (logging) fprintf(log_file, "%02X ", a[i]);
		} else {
			if (isascii(a[i])) {
				printf("%c", a[i]);
				if (logging) if (logging) fprintf(log_file, "%c", a[i]);
			} else {
				switch (a[i]) {
					case '\n': printf("\\n"); if (logging) fprintf(log_file, "\\n"); break;
					case '\r': printf("\\r"); if (logging) fprintf(log_file, "\\r"); break;
					case '\t': printf("\\t"); if (logging) fprintf(log_file, "\\t"); break;
					default:   printf("\\x%02x", a[i]); if (logging) fprintf(log_file, "\\x%02x"); break;
				}
			}
		}
	}
	printf("\n");
	if (logging) fprintf(log_file, "\n");
}

void sniff_packets(int client_fd, int server_fd) {

	unsigned char buffer[buffer_size];
	int numbytes;
	
	while (1) {
		//Read from client
		numbytes = read(client_fd, buffer, buffer_size);
		if (numbytes > 0) {
		
			//Log bytes sent by client
			printf("C → S %d bytes: ", numbytes);
			if (logging) fprintf(log_file, "C → S %d bytes: ", numbytes);
			dumparr(buffer, numbytes);
			
			//Send bytes to server
			write(server_fd, buffer, numbytes);
		}
		
		//Read form server
		numbytes = read(server_fd, buffer, buffer_size);
		if (numbytes > 0) {
			
			//Log bytes sent by server
			printf("S → C %d bytes: ", numbytes);
			if (logging) fprintf(log_file, "S → C %d bytes: ", numbytes);
			dumparr(buffer, numbytes);
			
			//Send bytes to client
			write(client_fd, buffer, numbytes);
		}
	}
	
}

int main(int argc, char **argv) {
	srand(time(0));
	
	parse_options(argc, argv);
	
	if (client_port == -1) client_port = 50000 + rand() % 1000 * 10;
	
	int proxy_fd = init_client();
	int server_fd = init_server();
	
	printf("Okay, created proxy C %d ←→ S %d.\n", client_port, server_port);
	
	printf("Press return when ready...\n");
	getchar();
	printf("Starting proxy.\n");
	
	//Start communicating with client
	struct sockaddr_in server_addr = (struct sockaddr_in){AF_INET, htons(server_port), 0};
	int client_fd = accept(proxy_fd, NULL, NULL);
	
	//Start communicating with server
	connect(server_fd, &server_addr, sizeof server_addr);
	
	//Set sockets to be nonblocking
	fcntl(client_fd, F_SETFL, O_NONBLOCK);
	fcntl(server_fd, F_SETFL, O_NONBLOCK);
	
	sniff_packets(client_fd, server_fd);
}
